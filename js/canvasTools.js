//Common tools for working with canvas and it's elements

//Function adds images to canvas. Params - canvas, variation, boolean array whether image is movable or not
function addToCanvas(canvas, variation, element) {
    fabric.Image.fromURL(variation.img, function(img) {
        //var img2 = new Image();
        //img2.crossOrigin = "Anonymous";
        //variation.img.crossOrigin= "Anonymous";
        img.variation = variation;
       // img.variation.crossOrigin = 'anonymous';
        //img.variation.img.crossOrigin = 'anonymous';

        var cWidth = canvas.getWidth();
        var cHeight = canvas.getHeight();
        var iWidth = img.getWidth();
        var iHeight = img.getHeight();

        img.hasControls = false;
        img.hasBorders = element.isMovable;
        img.evented = element.isMovable;
       // img.crossOrigin = "Anonymous"; //$(img).attr('crossOrigin', 'anonymous'); //setAttribute('crossOrigin', 'anonymous');


        if(variation.top) {
            img.setTop(variation.top);
        } else {
            img.setTop((cHeight - iHeight) / 2);
        }

        if(variation.left) {
            img.setLeft(variation.left);
        } else {
            img.setLeft((cWidth - iWidth) / 2);
        }
        var elem = new CanvasElement(img, element["z-index"],element.name, variation.name);
        img.name = element.name;
        canvas.add(img);
        canvas.elems.push(elem);
        onLoad(canvas);
    }, {"crossOrigin": "*"});
};

var CanvasElement = function(canvasObject, zIndex, elementName, elementVariationName) {
    this.canvasObject = canvasObject;
    this.zIndex = zIndex;
    this.elementName = elementName;
    this.elementVariationName = elementVariationName;
};

function onLoad(canvas) {
    if(canvas.getObjects().length === canvas.elementCount){
        var elems = canvas.elems;
        elems.sort(function(a,b){
            return a.zIndex > b.zIndex;
        });

        for(var i = 0; i < elems.length; i++){
            canvas.bringToFront(elems[i].canvasObject);
        }

        hideSpin();
    }
}
$.fn.spin.presets.flower = {
    lines:   9
    , length: 20
    , width:  6
    , radius:  10
}

/**
 * Redraws canvas
 * @param canvas - canvas to redraw
 * @param product - product to draw
 */
function redrawCanvas(canvas, product) {
    if(!canvas.product || canvas.product.name != product.name) {
        fullRedraw(canvas, product);
    } else {
        elementsRedraw(canvas);
    }
}

function fullRedraw(canvas, product){
    showSpin();
    canvas.clear().renderAll();
    canvas.product = product;
    canvas.elems = [];
    canvas.loaded = 0;
    canvas.elementCount = product.elements.length;

    product.elements.forEach(function(element){
        var variations = element.elementVariations;
        for(var i = 0; i <  variations.length; i++) {
            if(variations[i].selected){
                addToCanvas(canvas, variations[i], element);
                break;
            }
        }
    });
}


function elementsRedraw(canvas) {
    var product = canvas.product;
    product.elements.forEach(function(element){
        var variations = element.elementVariations;
        for(var i = 0; i <  variations.length; i++) {
            if(variations[i].selected) {
                var elemIndex = getElementByNameIndex(canvas.elems, element.name);
                if(elemIndex != -1) {
                    var canvasElem = canvas.elems[elemIndex];
                    if(canvasElem.elementVariationName != variations[i].name) {
                        showSpin();
                        canvas.remove(canvasElem.canvasObject);
                        canvas.elems.splice(elemIndex, 1);
                        addToCanvas(canvas, variations[i], element);
                        break;
                    }

                }
            }
        }
    });
}
function getElementByNameIndex(array, name){
    for(var i = 0; i < array.length; i++){
        if(array[i].elementName === name){
            return i;
        }
    }
    return -1;
}


/*function downloadFabric(url,name){
// make the link. set the href and download. emulate dom click
    $('#newton-download-emulator').attr({href:url,download:name})[0].click();
}*/
function downloadFabric(canvas){
    canvas.deactivateAll().renderAll();
    window.open(canvas.toDataURL());
}

/*function downloadFabric(canvas,name){
    canvas.deactivateAll().renderAll();
//  convert the canvas to a data url and download it.
    $('#newton-download-emulator').attr({href:canvas.toDataURL(),download:name+'.png'})[0].click();

    //download(canvas.toDataURL(),name+'.png');
}*/