var gulp = require('gulp');
var webserver = require('gulp-webserver');
var concat = require('gulp-concat');
var less = require('gulp-less');
var path = require('path');
var clean = require('gulp-clean');


gulp.task('webserver', function() {
    gulp.src('./')
        .pipe(webserver({
            livereload: true,
            directoryListing: true,
            open: "/index.html",
            port: 3001
        }));
});

gulp.task('webserver2', function() {
    gulp.src('./')
        .pipe(webserver({
            livereload: true,
            directoryListing: true,
            open: "/index.html",
            port: 3001,
            host: "0.0.0.0"
        }));
});

gulp.task('less', function () {
    return gulp.src('./less/**/*.less')
        .pipe(less({
            paths: [ path.join(__dirname, 'less', 'includes') ]
        }))
        .pipe(gulp.dest('./out/styles/'));
});

gulp.task('less-concat', ['less'], function() {
    return gulp.src('./out/styles/*.css')
        .pipe(concat('all.css'))
        .pipe(gulp.dest('./out/'));
});


gulp.task('js-concat', function() {
    return gulp.src('./js/*.js')
        .pipe(concat('all.js'))
        .pipe(gulp.dest('./out/'));
});

gulp.task('directives-concat', function() {
    return gulp.src('./angular/directives/*.js')
        .pipe(concat('all.directives.js'))
        .pipe(gulp.dest('./out/'));
});

gulp.task('watch', function() {
    gulp.watch('./js/*.js', ['js-concat']);
    gulp.watch('./less/*.less', ['less', 'less-concat']);
    gulp.watch('./angular/directives/*.js', ['directives-concat']);
});

gulp.task('clean', function () {
    return gulp.src('out/*', {read: false})
        .pipe(clean());
});

// Default Task
gulp.task('default', ['build','webserver']);
gulp.task('server', ['build','webserver2']);


gulp.task('build', ['js-concat', 'directives-concat', 'less', 'less-concat', 'watch']);